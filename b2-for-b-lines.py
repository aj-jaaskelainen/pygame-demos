import pygame
from pygame.locals import *

# Initialize
pygame.init()
pygame.display.set_caption("Basic 1 - background")
canvas = pygame.display.set_mode((800, 600), pygame.HWSURFACE | pygame.DOUBLEBUF)

# Drawing variables
focus = (0,300)

# Draw lines from focus point
for y in range(10,600,20):
    pygame.draw.line(canvas, (0, 100, 50), focus, (750,y))

# Make drawings show in screen.
pygame.display.update()

# Wait until we quit
running = True
while(running):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

# Cleanup
pygame.quit()