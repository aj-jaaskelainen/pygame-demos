import pygame
from pygame.locals import *

# Initialize
pygame.init()
pygame.display.set_caption("Basic 3 - render function")
canvas_size = (800, 600)
canvas = pygame.display.set_mode(canvas_size, pygame.HWSURFACE | pygame.DOUBLEBUF)

box_x = 20
box_y = 300
box_size = (40,40)
box_color = (200,200,150)

started = False

def onRender():
    global box_color, box_x, box_y, box_size, canvas_size
    # Clear the screen by drawing background
    pygame.draw.rect(canvas, (0,0,64), ((0,0), canvas_size))
    pygame.draw.rect(canvas, box_color, ((box_x, box_y), box_size))

def onLoop():
    global box_x, started
    if started:
        box_x += 1

# Wait until we quit
running = True
while(running):
    # Handle events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == K_SPACE:
                started = not started
    # Loop and render
    onLoop()
    onRender()
    # Make drawings show in screen.
    pygame.display.update()

# Cleanup
pygame.quit()