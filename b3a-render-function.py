import pygame
from pygame.locals import *

# Initialize
pygame.init()
pygame.display.set_caption("Basic 3 - render function")
canvas = pygame.display.set_mode((800, 600), pygame.HWSURFACE | pygame.DOUBLEBUF)

box_x = 400
box_y = 300
box_size = (40,40)
box_color = (200,200,150)

def onRender():
    pygame.draw.rect(canvas, box_color, ((box_x, box_y), box_size))

# Wait until we quit
running = True
while(running):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    onRender()
    # Make drawings show in screen.
    pygame.display.update()

# Cleanup
pygame.quit()