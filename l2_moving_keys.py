import pygame
from pygame.locals import *

class Mover:
    def __init__(self, pt):
        self.X = pt[0]
        self.Y = pt[1]
        self.size = (20,20)
        self.color = (0,200,0)
 
    def move(self, x, y):
        self.X += x
        self.Y += y

    def draw(self, canvas):
        pygame.draw.rect(canvas, self.color, ((self.X,self.Y), self.size))

class App:
    def __init__(self):
        self._running = True
        self._canvas = None
        self.size = self.width, self.height = 1000, 800
 
    def on_init(self):
        pygame.init()
        self._canvas = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        pygame.display.set_caption("Drawing elements")
        self._running = True
        self.mover = Mover((500,400))

    def on_event(self, event):
        if event.type == pygame.QUIT:
            self._running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                self.mover.move(-10,0)
            if event.key == pygame.K_RIGHT:
                self.mover.move(10,0)
            if event.key == pygame.K_UP:
                self.mover.move(0,-10)
            if event.key == pygame.K_DOWN:
                self.mover.move(0,10)

    def on_loop(self):
        pass

    def on_render(self):
        pygame.draw.rect(self._canvas, (0,0,64), ((0,0), self.size))
        self.mover.draw(self._canvas)
        pygame.display.flip()

    def on_cleanup(self):
        pygame.quit()
 
    def on_execute(self):
        if self.on_init() == False:
            self._running = False 
        while( self._running ):
            for event in pygame.event.get():
                self.on_event(event)
            self.on_loop()
            self.on_render()
        self.on_cleanup()
 
if __name__ == "__main__" :
    theApp = App()
    theApp.on_execute()