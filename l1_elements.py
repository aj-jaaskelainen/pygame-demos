import pygame
from pygame.locals import *
 
class App:
    def __init__(self):
        self._running = True
        self._canvas = None
        self.size = self.width, self.height = 1000, 800
 
    def on_init(self):
        pygame.init()
        self._canvas = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        pygame.display.set_caption("Drawing elements")
        self._running = True
 
    def on_event(self, event):
        if event.type == pygame.QUIT:
            self._running = False

    def on_loop(self):
        pass

    def on_render(self):
        # Drawing lines accors canvas
        pygame.draw.line(self._canvas, (0,0,200), (0,400), (999,400))
        pygame.draw.line(self._canvas, (0,0,200), (500,0), (500,799))
        # Surrounding rect
        pygame.draw.rect(self._canvas, (0,200,0), ((1,1), (998,798)), 3)
        # Circle in the middle
        pygame.draw.circle(self._canvas, (200,0,0), (500,400), 40, 2)
        pygame.display.update()

    def on_cleanup(self):
        pygame.quit()
 
    def on_execute(self):
        if self.on_init() == False:
            self._running = False 
        while( self._running ):
            for event in pygame.event.get():
                self.on_event(event)
            self.on_loop()
            self.on_render()
        self.on_cleanup()
 
if __name__ == "__main__" :
    theApp = App()
    theApp.on_execute()