PYGAME-DEMOS
Python Game (pygame) module short demos

# Coding

NOTE: run `source ./penv/bin/activate` from console before running pygame programs.
This activates the python virtual environment so that the pygame module is found
by python.

PyGame document: https://www.pygame.org/docs/

Python documentation
- library: https://docs.python.org/3.11/library/index.html
- tutorial: https://www.w3schools.com/python/default.asp


# Installation & setup

## Iterm
Get and install iTerm2: https://iterm2.com/downloads.html

Setup:
- Use white background :-)

## Mac Ports
Get and install Mac Ports: https://www.macports.org/install.php

Setup:
- Open Iterm console
- Install packages: git, python3

Use "sudo port install [package name]" -command to install.

## Get demo code

Create local directory for Python projects. Since MacOS may synchronize Document-directory into the iCloud (depending on your settings), it is recommended that you create this directory under your home, e.g. [home]/code/python.

Once you have the directory. Clone demos into it.

	cd ~/code/python
	git clone https://github.com/jaaskelainen-aj/pygame-demos.git

## Create python environment (once)

pygame-demo uses python virtual environment to make sure code is comptatible with particular version of the pygame module. Demo files includes a file 'requirements.txt' that tells which python modules are needed. Lets create the environment, activate it and initialize it with modules.

	cd pygame-demo
	python -m venv penv
	source ./penv/bin/activate
	python -m ensurepip --upgrade
	python -m pip install -r requirements.txt
