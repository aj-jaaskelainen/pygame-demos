import pygame
from pygame.locals import *

# Initialize
pygame.init()
pygame.display.set_caption("Basic 1 - background")
canvas = pygame.display.set_mode((800, 600), pygame.HWSURFACE | pygame.DOUBLEBUF)

# set drawing variables
rsize = (40,40)
y = 20
green = 100

for nx in range(10):
    x = 20
    blue = 100
    for kx in range(10):
        pygame.draw.rect(canvas, (0, green, blue), ((x, y), rsize))
        x += 50
        blue += 10
    y += 50
    green += 10

# Make drawings show in screen.
pygame.display.update()

# Wait until we quit
running = True
while(running):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

# Cleanup
pygame.quit()