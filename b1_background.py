import pygame
from pygame.locals import *

# Initialize
pygame.init()
pygame.display.set_caption("Basic 1 - background")
canvas = pygame.display.set_mode((800, 600), pygame.HWSURFACE | pygame.DOUBLEBUF)

# Load background
bgdn = pygame.image.load("gradient_bgnd.jpg").convert()

running = True
while(running):
    # Draw the background = erase
    canvas.blit(bgdn, (0,0))
    # Handle events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    # Draw items
    # ... none ...
    # Make drawings show in screen.
    pygame.display.update()

# Cleanup
pygame.quit()