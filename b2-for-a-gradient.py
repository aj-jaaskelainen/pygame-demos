import pygame
from pygame.locals import *

# Initialize
pygame.init()
pygame.display.set_caption("Basic 1 - background")
canvas = pygame.display.set_mode((800, 600), pygame.HWSURFACE | pygame.DOUBLEBUF)

# Drawing variables
rsize = (40,60)
x = 20
blue = 100

# Blue gradients
for nx in range(10):
    pygame.draw.rect(canvas, (0, 100, blue), ((x, 50), rsize))
    x += 50
    blue += 15

# Green gradients
x = 20
for green in range(100, 200, 10):
    pygame.draw.rect(canvas, (0, green, 100), ((x, 120), rsize))
    x += 50

# Make drawings show in screen.
pygame.display.update()

# Wait until we quit
running = True
while(running):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

# Cleanup
pygame.quit()