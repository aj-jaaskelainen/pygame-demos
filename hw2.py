import pygame
from pygame.locals import *

SCREEN_W = 1000
SCREEN_H = 800

class Note:
    def __init__(self, font_name, font_size, size):
        self.font = pygame.font.SysFont(font_name, font_size)
        x = (SCREEN_W-size[0])/2
        self.rect = pygame.Rect((x, 5, x+size[0], 5+size[1]))
        self.text = None
    
    def SetText(self, text):
        self.text = text

    def Draw(self, screen):
        if self.text:
            rt = self.font.render(self.text, True, (0,0,255))
            screen.blit(rt, (10, 10))

class App:
    def __init__(self):
        self._running = True
        self.size = (SCREEN_W, SCREEN_H)
        pygame.init()
        pygame.display.set_caption("PyGame Hello World 2")
        self.screen = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        self.note = Note('helvetica', 16, (400, 40))
  
    def on_event(self, event):
        if event.type == pygame.QUIT:
            self._running = False
        if event.type == pygame.KEYDOWN:
            if event.key == 104:
                self.note.SetText("Hello World")

    def on_loop(self):
        pass

    def on_render(self):
        self.note.Draw(self.screen)
        pygame.display.update()

    def on_cleanup(self):
        pygame.quit()
 
    def on_execute(self):
        while( self._running ):
            for event in pygame.event.get():
                self.on_event(event)
            self.on_loop()
            self.on_render()
        self.on_cleanup()
 
if __name__ == "__main__" :
    theApp = App()
    theApp.on_execute()
